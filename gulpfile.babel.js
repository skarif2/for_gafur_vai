import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import path from 'path';
import del from 'del';
import { exec } from 'child_process';
import runSequence from 'run-sequence';

const debug = require('debug')('al-islam:gulpfile.babel');

const plugins = gulpLoadPlugins();
const deployServer = 'server address'; // i.e. 'root@139.59.26.76';
const deployPath = 'folder location'; // i.e. '~/al-islam';
// const restartId = 1;

const paths = {
  js: ['./**/*.js', '!dist/**', '!node_modules/**', '!coverage/**'],
  nonJs: ['./package.json', './.gitignore'],
  tests: './server/tests/*.js'
};

// Clean up dist and coverage directory
gulp.task('clean', () =>
  del(['dist/**', 'coverage/**', '!dist', '!coverage'])
);

// Copy non-js files to dist
gulp.task('copy', () =>
  gulp.src(paths.nonJs)
    .pipe(plugins.newer('dist'))
    .pipe(gulp.dest('dist'))
);

// Compile ES6 to ES5 and copy to dist
gulp.task('babel', () =>
  gulp.src([...paths.js, '!gulpfile.babel.js'], { base: '.' })
    .pipe(plugins.newer('dist'))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.babel())
    .pipe(plugins.sourcemaps.write('.', {
      includeContent: false,
      sourceRoot(file) {
        return path.relative(file.path, __dirname);
      }
    }))
    .pipe(gulp.dest('dist'))
);

// Start server with restart on file changes
gulp.task('nodemon', ['copy', 'babel'], () =>
  plugins.nodemon({
    script: path.join('dist', 'index.js'),
    ext: 'js',
    ignore: ['node_modules/**/*.js', 'dist/**/*.js'],
    tasks: ['copy', 'babel']
  })
);

// rsync dist folder to server
gulp.task('rsyncify', ['copy', 'babel'], () =>
  exec(`rsync -r ./dist/ ${deployServer}:${deployPath}`, (err) => {
    if (err) {
      debug(err);
    } else {
      debug('rsync done...');
      // exec(`ssh root@188.166.180.131 pm2 restart ${restartId}`, (e) => {
      //   if (e) debug(e);
      //   debug('restart done...');
      // });
    }
  })
);

// deploy dist folder to server
gulp.task('deploy', ['clean'], () => runSequence('rsyncify'));

// gulp serve for development
gulp.task('serve', ['clean'], () => runSequence('nodemon'));

// default task: clean dist, compile js files and copy non-js files.
gulp.task('default', ['clean'], () => {
  runSequence(
    ['copy', 'babel']
  );
});
