function getImageName(base64) {
  let imageExtention = 'jpg';

  if (base64.charAt(0) === '/') {
    imageExtention = 'jpeg';
  } else if (base64.charAt(0) === 'R') {
    imageExtention = 'gif';
  } else if (base64.charAt(0) === 'i') {
    imageExtention = 'png';
  }

  return `${randomString()}${randomString()}${randomString()}.${imageExtention}`;
}

function randomString() {
  return Math.floor((1 + Math.random()) * 0x1000000)
      .toString(16)
      .substring(1);
}

export default { getImageName, randomString };
