import Promise from 'bluebird';
import path from 'path';
import fs from 'fs';

function saveImage(base64, imageName) {
  let image = base64;
  const publicDir = path.join(__dirname, '/../../public');
  const imageDir = `${publicDir}/users`;
  return new Promise((resolve, reject) => {
    const imagePath = `${imageDir}/${imageName}`;

    // image = image.replace(/^(.*)base64,/, "");
    image = image.replace(/ /g, '+');

    if (!fs.existsSync(imageDir)) {
      fs.mkdirSync(publicDir);
      fs.mkdirSync(imageDir);
    }
    fs.writeFile(imagePath, image, 'base64', (err) => {
      if (err) reject('failed to save image');
      else resolve(imageName);
    });
  });
}

function deleteImage(imageName) {
  const pathValue = `${__dirname}/../public/users/${imageName}`;
  return new Promise((resolve, reject) => {
    fs.unlink(pathValue, (err) => {
      if (err) reject('failed to delete image');
      else resolve(imageName);
    });
  });
}

export default { saveImage, deleteImage };
