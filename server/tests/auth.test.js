import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

describe('## Auth APIs', () => {
  let token = 'abcd';
  let user = {
    name: 'KK123',
    email: 'kk@kkgroup.com',
    password: 'kk123',
    deviceId: 'test',
    gender: 'male',
    birthDate: '1990-10-10'
  };

  const userAuth = {
    email: 'kk@kkgroup.com',
    password: 'kk123',
    deviceId: 'test'
  };

  before((done) => {
    request(app)
      .post('/api/users')
      .send(user)
      .expect(httpStatus.OK)
      .then(done());
  });

  describe('# POST /api/auth/login', () => {
    it('should let user login to the system', (done) => {
      request(app)
        .post('/api/auth/login')
        .send(userAuth)
        .expect(httpStatus.OK)
        .then((res) => {
          token = res.body.token;
          expect(res.body.user.email).to.equal(user.email);
          user = res.body.user;
          done();
        })
        .catch(done);
    });
  });

  after((done) => {
    request(app)
      .delete(`/api/users/${user._id}`)
      .set('Authorization', `Bearer ${token}`)
      .expect(httpStatus.OK)
      .then(done());
  });
});
