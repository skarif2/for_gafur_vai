import omit from 'lodash.omit';
import mongoose from 'mongoose';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../index';

chai.config.includeStack = true;

/**
 * root level hooks
 */
after((done) => {
  // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.close();
  done();
});

describe('## User APIs', () => {
  let token = 'abcd';
  let user = {
    name: 'KK123',
    email: 'kk@kkgroup.com',
    password: 'kk123',
    deviceId: 'test',
    gender: 'male',
    birthDate: '1990-10-10'
  };
  const userAuth = {
    email: 'kk@kkgroup.com',
    password: 'kk123',
    deviceId: 'test'
  };

  describe('# POST /api/users', () => {
    it('should create a new user', (done) => {
      request(app)
        .post('/api/users')
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.name).to.equal(user.name);
          expect(res.body.email).to.equal(user.email);
          expect(res.body.gender).to.equal(user.gender);
          expect(res.body.birthdate).to.equal(user.birthdate);
          user = omit(res.body, 'image');
          done();
        })
        .catch(done);
    });
  });

  describe('# POST /api/auth/login', () => {
    it('should let new user login to the system', (done) => {
      request(app)
        .post('/api/auth/login')
        .send(userAuth)
        .expect(httpStatus.OK)
        .then((res) => {
          token = res.body.token;
          expect(res.body.user.email).to.equal(user.email);
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /api/users/:userId', () => {
    it('should get user details', (done) => {
      request(app)
        .get(`/api/users/${user._id}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.name).to.equal(user.name);
          expect(res.body.email).to.equal(user.email);
          expect(res.body.gender).to.equal(user.gender);
          expect(res.body.birthdate).to.equal(user.birthdate);
          done();
        })
        .catch(done);
    });

    it('should report error with message - Not found, when user does not exists', (done) => {
      request(app)
        .get('/api/users/56c787ccc67fc16ccc1a5e92')
        .set('Authorization', `Bearer ${token}`)
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body.message).to.equal('Not Found');
          done();
        })
        .catch(done);
    });
  });

  describe('# PUT /api/users/:userId', () => {
    it('should update user details', (done) => {
      user.name = 'KK';
      request(app)
        .put(`/api/users/${user._id}`)
        .set('Authorization', `Bearer ${token}`)
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.name).to.equal('KK');
          expect(res.body.email).to.equal(user.email);
          expect(res.body.gender).to.equal(user.gender);
          expect(res.body.birthdate).to.equal(user.birthdate);
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /api/users/', () => {
    it('should get all users', (done) => {
      request(app)
        .get('/api/users')
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('array');
          done();
        })
        .catch(done);
    });
  });

  describe('# DELETE /api/users/', () => {
    it('should delete user', (done) => {
      request(app)
        .delete(`/api/users/${user._id}`)
        .set('Authorization', `Bearer ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.name).to.equal('KK');
          expect(res.body.email).to.equal(user.email);
          expect(res.body.gender).to.equal(user.gender);
          expect(res.body.birthdate).to.equal(user.birthdate);
          done();
        })
        .catch(done);
    });
  });
});
