import omit from 'lodash.omit';
import httpStatus from 'http-status';
import bcrypt from 'bcryptjs';
import APIError from '../helpers/APIError';
import IMGHandler from '../helpers/IMGHandler';
import RNDNumber from '../helpers/RNDNumber';
import User from '../models/user.model';

const salt = bcrypt.genSaltSync(10);

/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  User.get(id)
    .then((user) => {
      req.userDetail = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get user
 * @returns {User}
 */
function get(req, res, next) {
  const user = req.userDetail;
  if (req.user.email !== user.email) {
    const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED);
    return next(err);
  }
  return res.json(user);
}

/**
 * Create new user
 * @property {string} req.body.name - The name of user.
 * @property {string} req.body.email - The email of user.
 * @property {string} req.body.password - The password of user.
 * @property {string} req.body.image - The base64 image of user.
 * @property {string} req.body.gender - The gender of user.
 * @property {string} req.body.birthDate - The birthDate of user.
 * @property {string} req.body.location - The location of user.
 * @returns {User}
 */
function create(req, res, next) {
  const userParams = {
    name: req.body.name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, salt),
    deviceId: req.body.deviceId,
    regCode: req.body.regCode
  };
  if (req.body.image) {
    userParams.image = RNDNumber.getImageName(req.body.image);
  } else {
    userParams.image = 'default.jpeg';
  }
  if (req.body.gender) userParams.gender = req.body.gender;
  if (req.body.birthDate) userParams.birthDate = new Date(req.body.birthDate);
  if (req.body.location) userParams.location = req.body.location;

  const user = new User(userParams);
  user.save()
    .then((savedUser) => {
      const userDetail = omit(savedUser.toObject(), ['password', 'createdAt', '__v']);
      if (req.body.image) IMGHandler.saveImage(req.body.image, userParams.image);
      res.json(userDetail);
    })
    .catch((e) => {
      next(e);
    });
}

/**
 * Update existing user
 * @property {string} req.body.name - The name of user.
 * @property {string} req.body.email - The email of user.
 * @property {string} req.body.password - The password of user.
 * @property {string} req.body.image - The image of user.
 * @property {string} req.body.gender - The gender of user.
 * @property {string} req.body.birthDate - The birthDate of user.
 * @property {string} req.body.location - The location of user.
 * @returns {User}
 */
function update(req, res, next) {
  let user = req.userDetail;

  if (req.user.email !== user.email) {
    const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED);
    next(err);
    return;
  }

  user.name = req.body.name ? req.body.name : user.name;
  user.email = req.body.email ? req.body.email : user.email;
  user.gender = req.body.gender ? req.body.gender : user.gender;
  user.regCode = req.body.regCode ? req.body.regCode : user.regCode;
  user.password = req.body.password ? req.body.password : user.password;
  user.birthDate = req.body.birthDate ? req.body.birthDate : user.birthDate;
  user.image = req.body.image ? RNDNumber.getImageName(req.body.image) : user.image;

  user.save()
    .then((savedUser) => {
      const userDetail = omit(savedUser.toObject(), ['password', 'createdAt', '__v']);
      if (req.body.image) {
        IMGHandler.saveImage(req.body.image, user.image);
        IMGHandler.deleteImage(req.userDetail.image);
      }
      res.json(userDetail);
    })
    .catch(e => next(e));
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {User[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  User.list({ limit, skip })
    .then(users => res.json(users))
    .catch(e => next(e));
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.userDetail;
  if (req.user.email !== user.email) {
    const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED);
    next(err);
    return;
  }
  user.remove()
    .then(deletedUser => res.json(deletedUser))
    .catch(e => next(e));
}

export default { load, get, create, update, list, remove };
