import omit from 'lodash.omit';
import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import bcrypt from 'bcryptjs';
import APIError from '../helpers/APIError';
import User from '../models/user.model';

const config = require('../../config/env');

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function loadUser(req, res, next) {
  User.getByPhoneNumber(req.body.phoneNumber)
    .then((user) => {
      req.user = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Returns jwt token if valid email and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {token, user}
 */
function login(req, res, next) {
  // user had already been fetched at route level
  // Idea here was to send jwt if all credentials are ok
  if (req.body.phoneNumber === req.user.phoneNumber &&
      bcrypt.compareSync(req.body.password, req.user.password)) {
    const token = jwt.sign({
      phoneNumber: req.user.phoneNumber,
      deviceId: req.body.deviceId
    }, config.jwtSecret);
    const user = omit(req.user.toObject(), ['password', 'createdAt', '__v']);
    return res.json({ token, user });
  }

  const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED);
  return next(err);
}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  // req.user is assigned by jwt middleware if valid token is provided
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

export default { loadUser, login, getRandomNumber };
