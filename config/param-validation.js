import Joi from 'joi';

export default {
  // POST /api/users
  createUser: {
    body: {
      name: Joi.string().required(),
      email: Joi.string().required(),
      password: Joi.string().required(),
      deviceId: Joi.string().required(),
      regCode: Joi.string().required(),
      phoneNumber: Joi.string().required()
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      name: Joi.string(),
      email: Joi.string(),
      password: Joi.string(),
      image: Joi.string(),
      gender: Joi.string(),
      birthDate: Joi.date()
    },
    params: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      phoneNumber: Joi.string().required(),
      password: Joi.string().required(),
      deviceId: Joi.string().required()
    }
  }
};
